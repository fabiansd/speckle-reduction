import matplotlib.pyplot as plt
import numpy
import pickle
import sys


try:
    historyfile = sys.argv[1]
except IndexError:
    print("Usage: plothistory.py <path/historyfile>")
    sys.exit(1)# start the program

try:
	history = pickle.load( open( historyfile, "rb" ) )
except FileNotFoundError:
	print('Could not find file from the path ' + historyfile)
	sys.exit(1)

	

# summarize history for loss
plt.plot(history['loss'][0:70])
plt.plot(history['val_loss'][0:70])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['loss', 'val_loss'], loc='upper left')

plt.savefig(historyfile+'.jpg',format='jpg')
plt.show()