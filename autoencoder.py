


from keras.layers import *
from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.models import Model, load_model
from keras import backend as k
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping,ModelCheckpoint
from keras.regularizers import l2
from keras.models import model_from_json

from sklearn.model_selection import train_test_split


import numpy as np
from dataloader import Dataloader
from PIL import Image
import matplotlib.pyplot as plt
import _pickle as pickle
import os





class Autoencoder:

    def __init__(self, trainfolderpath,labelfolderpath,imagetype, imagesize = (None,None)):
        
        self.imagesize = imagesize
        self.imagetype = imagetype
        #self.kernel_size = 3
        self.leaky_alpha = 0.2
        #self.encoder_stride = 2
        #self.decoder_stride = 2
        self.l2_reg = 0.0001

        self.historytemp = None


        print('Getting data')
        print('Training folder path:',trainfolderpath)
        print('Label folder path',labelfolderpath)
        self.data = Dataloader(w_label = True, trainpath=trainfolderpath,labelpath=labelfolderpath,imagetype=imagetype,image_size=imagesize)
        self.traindata,self.labeldata = self.data.returndata()
        self.traindata = self.uint82float(self.traindata)
        self.labeldata = self.uint82float(self.labeldata)

        self.traingenerator = ImageDataGenerator()
        self.validationgenerator = ImageDataGenerator()

        self.validation_split = 0.8
        self.random_state = 42

        self.train_train_s, self.train_valid_s, self.label_train_s, self.label_valid_s = train_test_split(
            self.traindata, self.labeldata, test_size=self.validation_split, random_state=self.random_state)


        print('The shape of training and label data is' , self.traindata.shape,' and ',self.labeldata.shape)
        print('Traindata size: %d Mb and Labeldata size: %d Mb' % (self.traindata.nbytes/1000000,self.labeldata.nbytes/1000000))

        self.n = self.data.returndatasize()


    def train(self, epochs, batch_size, optimizer = 'adadelta', loss='binary_crossentropy'):
        

        early_stopping = EarlyStopping(monitor='val_loss', patience=15)
        # checkpoint = ModelCheckpoint('checkpoint_model.h5', monitor='val_loss', verbose=0, save_best_only=True, mode='min')

        #sgd = optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
        #optimizer = optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.005)

        self.model.compile(optimizer=optimizer,loss=loss)
        print('Model compiled')

        if self.n*self.validation_split%batch_size != 0 and self.n-self.validation_split*self.n%batch_size != 0:
            print('Warning, validation split not divisible with batch size')
            #print('Do you wish to continue?')
            #i=input('Press enter to continue or n to exit')
            #if i == 'n':
            #    exit(0)


        log = self.model.fit_generator(
            self.traingenerator.flow(self.train_train_s, self.label_train_s, batch_size=batch_size), \
            steps_per_epoch=int(self.n * self.validation_split / batch_size), \
            validation_data=self.validationgenerator.flow(self.train_valid_s, self.label_valid_s,
                                                          batch_size=batch_size), \
            validation_steps=int(self.n - self.validation_split * self.n) / batch_size, \
            epochs=epochs, shuffle=True, callbacks=[early_stopping])
        
        #log = self.model.fit(self.traindata,self.labeldata, batch_size = batch_size, epochs=epochs, batch_size=batch_size, validation_split=0.2,shuffle=shuffle, callbacks=[early_stopping])
        
        print(log.history)

        self.historytemp = log.history

        
        print('--Model trained--')

    def float2uint8(self,input):
        return (((np.clip(input,0,1) / input.max()) * 255).astype('uint8'))

    def uint82float(self,input):
        return input.astype('float32')/255.

    def testpredictions(self,testfolderpath,testlabelfolderpath,imagetype,imagesize,folder):
        directory = os.path.join('result_samples',folder)
        if not os.path.exists(directory):
            os.makedirs(directory)

        if self.historytemp != None:
            with open(os.path.join(directory,'results.pickle'), 'wb') as f:
                    pickle.dump(self.historytemp, f)

        self.save_h5(folder)

        self.testdata = Dataloader(w_label = True, trainpath=testfolderpath,labelpath=testlabelfolderpath,\
                               imagetype=imagetype,image_size=imagesize)


        self.test_traindata,self.test_labeldata = self.testdata.returndata()
        import time
        t0 = time.time()
        print(self.test_traindata.shape)
        self.predictions = self.model.predict(self.uint82float(self.test_traindata))

        t1 = time.time()
        total = t1 - t0
        print('total predicting time %.3f' % total)

        self.predictions = self.float2uint8(self.predictions)

        self.saveimages(self.predictions,'prediction_',directory)
        self.saveimages(self.test_traindata,'input_',directory)
        self.saveimages(self.test_labeldata,'target_',directory)

        self.save_comparison(self.test_traindata, self.predictions, self.test_labeldata,directory)
        self.save_diff_org_ae(self.test_traindata,self.predictions,directory)


        self.sample = self.uint82float(self.test_traindata[0:1])
        t0 = time.time()
        self.sample = self.model.predict(self.sample)
        t1 = time.time()
        print('Timing for one prediction: %.3f' % (t1 - t0))


        print('Predictions tested and saved')

    def predict_video(self, videotrainpath, imagetype, imagesize, savevideofolder):

        videofolderpath = os.path.join('result_samples', savevideofolder, 'videoresults')
        if not os.path.exists(videofolderpath):
            os.makedirs(videofolderpath)

        self.videodata = Dataloader(w_label=False, trainpath=videotrainpath, labelpath=None, imagetype=imagetype,
                                    image_size=imagesize)

        self.predictions = self.videodata.returndata()
        trainfiles = self.videodata.return_trainfiles()

        self.predictions = self.predictions.astype('float32') / 255.

        self.predictions = self.model.predict(self.predictions)

        self.predictions = self.float2uint8(self.predictions)

        for i in range(len(self.predictions)):
            testimsave = Image.fromarray(self.predictions[i, :, :, 0])
            if testimsave.mode != 'RGB':
                testimsave = testimsave.convert('RGB')

            testimsave.save(os.path.join(videofolderpath, os.path.split(trainfiles[i])[-1]))

        print('Videostream predicted and saved')

    def saveimages(self,tensor,name,directory):

        for i in range(len(tensor)):
            testimsave = Image.fromarray(tensor[i,:,:,0])
            if testimsave.mode != 'RGB':
                testimsave = testimsave.convert('RGB')
            testimsave.save(os.path.join(directory,name + str(i) +'.'+self.imagetype))

    def save_comparison(self,test, pred, label,directory):

        for i in range(pred.shape[0]):
            conc = np.concatenate((test[i],pred[i],label[i]),axis=1)
            conc_im = Image.fromarray(conc[:,:,0],mode='L')
            #if conc_im.mode != 'RGB':
            #    conc_im = conc_im.convert('RGB')
            conc_im.save(os.path.join(directory,'org_ae_nlmf_' + str(i) + '.' + self.imagetype))

    def save_diff_org_ae(self,test,pred,directory):

        for i in range(test.shape[0]):
            diff = np.subtract(self.uint82float(test[i]),self.uint82float(pred[i]))
            diff = self.float2uint8(np.absolute(diff))
            conc = np.concatenate((test[i],pred[i],diff),axis=1)
            conc_im = Image.fromarray(conc[:,:,0],mode='L')
            conc_im.save(os.path.join(directory,'org_pred_diff_' + str(i) + '.' + self.imagetype))

    def save_diff_ae_label(self, pred, label,directory):

        for i in range(pred.shape[0]):
            diff = np.subtract(self.uint82float(pred[i]), self.uint82float(label[i]))
            diff = self.float2uint8(np.absolute(diff))
            conc = np.concatenate((pred[i], label[i], diff), axis=1)

            conc_im = Image.fromarray(conc[:,:,0], mode='L')
            conc_im.save(os.path.join(directory, 'pred_label_diff_' + str(i) + '.' + self.imagetype))


    def save_json(self):
        json_string = self.model.to_json()
        with open('model.json','w') as json_file:
            json_file.write(json_string)
        self.model.save_weights('model_weights.h5')

    def load_json(self):
        json_file = open('model.json','r')
        loaded_model_json = json_file.read()
        json_file.close()

        self.model = model_from_json(loaded_model_json)
        self.model.load_weights('model_weights.h5')
        self.model.summary()
        return self.model

    def save_h5(self,savepath=None):
        if savepath != None:
            self.model.save(os.path.join('result_samples',savepath,'model.h5'))
        else:
            self.model.save('model.h5')
    
    def load_h5(self,model_path):

        self.model = load_model(model_path)#, custom_objects={"tf": tf})
        self.model.summary()


    def show_image_comparison(self,index,indata1,indata2):
        test = self.float2uint8(indata1[index,:,:,0])
        test2 = self.float2uint8(indata2[index,:,:,0])

        testimage = Image.fromarray(test)
        test2image = Image.fromarray(test2)

        plt.figure(1)
        plt.subplot(1,2,1)
        plt.imshow(testimage,cmap='gray')
        plt.title('Training image')
        plt.axis('off')

        plt.subplot(1,2,2)
        plt.imshow(test2image,cmap='gray')
        plt.title('Target image')
        plt.axis('off')

        plt.tight_layout()
        plt.show()


    def show_prediction_result(self,index):

        test1 = ((self.traindata[index,0,:,:])*255.).astype('uint8')
        test2 = ((self.labeldata[index,0,:,:])*255.).astype('uint8')
        test3 = ((self.predictions[index,0,:,:])*255.).astype('uint8')
        
        testimage1 = Image.fromarray(test1)
        testimage2 = Image.fromarray(test2)
        testimage3 = Image.fromarray(test3)
        
        plt.figure(2)
        plt.subplot(1,3,1)
        plt.imshow(testimage1, cmap='gray')
        plt.title('Training image')
        plt.axis('off')

        plt.subplot(1,3,2)
        plt.imshow(testimage3, cmap='gray')
        plt.title('Predicted image')
        plt.axis('off')

        plt.subplot(1,3,3)
        plt.imshow(testimage2, cmap='gray')
        plt.title('Target image')
        plt.axis('off')

        plt.show()

    def code_block(self,x, filters):
        x = BatchNormalization()(x)
        x = Conv2D(filters,self.kernel_encoder, strides = self.encoder_stride, padding='same',kernel_regularizer=l2(self.l2_reg))(x)
        x = LeakyReLU(alpha=self.leaky_alpha)(x)

        return x

    def decode_block_upsample(self,x,filters,scale):
        x = Lambda(self.upsample_bicubic, arguments={'scale':scale})(x)
        x = BatchNormalization()(x)
        x = Conv2D(filters,self.kernel_encoder, strides = self.encoder_stride, padding='same',kernel_regularizer=l2(self.l2_reg))(x)
        x = LeakyReLU(alpha=self.leaky_alpha)(x)

        return x

    def decode_block(self,x,filters,scale):
        x = BatchNormalization()(x)
        x = Conv2DTranspose(filters,self.kernel_encoder,strides = self.encoder_stride, padding='same',kernel_regularizer=l2(self.l2_reg))(x)
        # x = Conv2D(filters,self.kernel_encoder, strides = 1, padding='same',kernel_regularizer=l2(self.l2_reg))(x)
        x = LeakyReLU(alpha=self.leaky_alpha)(x)

        return x


    def upsample_bicubic(self,input_tensor,scale=2):
        import tensorflow as tf
        H,W = input_tensor.shape[1],input_tensor.shape[2]
        return tf.image.resize_bilinear(input_tensor, [scale*H.value, scale*W.value])

    def create_fcn_transposed(self, filters, deep):
        self.kernel_encoder = 3
        self.encoder_stride = 1
        self.filters = filters

        shape = self.data.return_trainimage_shape()
        scale = 2

        input_tensor = Input(shape)
        print('input', k.int_shape(input_tensor))

        x = self.code_block(input_tensor, self.filters)

        x = self.code_block(x, self.filters)

        merge_1 = x

        x = self.code_block(x, self.filters)

        x = self.code_block(x, self.filters)

        merge_2 = x

        x = self.code_block(x, self.filters)

        if deep:

            x = self.code_block(x, self.filters)

            merge_3 = x

            x = self.code_block(x, self.filters)


            # bottleneck

            x = self.decode_block(x, self.filters, scale=scale)

            x = Add()([x, merge_3])

            x = self.decode_block(x, self.filters, scale=scale)

        x = self.decode_block(x, self.filters, scale=scale)

        x = Add()([x, merge_2])

        x = self.decode_block(x, self.filters, scale=scale)

        x = self.decode_block(x, self.filters, scale=scale)

        x = Add()([x, merge_1])

        x = self.decode_block(x, self.filters, scale=scale)

        x = self.decode_block(x, 1, scale=scale)

        x = Add()([x, input_tensor])

        x = Conv2D(1, 3, activation='sigmoid', padding='same', kernel_regularizer=l2(self.l2_reg))(x)


        self.model = Model(inputs=input_tensor, outputs=x)
        self.model.summary()
        print('Model created!')

    def create_fcn(self):
        self.kernel_encoder = 3
        self.encoder_stride = 1
        self.filters = 32

        shape = self.data.return_trainimage_shape()
        # self.init_filters = 32
        scale = 2

        input_tensor = Input(shape)
        print('input', k.int_shape(input_tensor))

        x = self.code_block(input_tensor, self.filters)

        x = self.code_block(x, self.filters)

        merge_1 = x

        x = self.code_block(x, self.filters)

        x = self.code_block(x, self.filters)

        # merge_2 = x

        # x = self.code_block(x, self.filters)

        # x = self.code_block(x, self.filters)

        # merge_3 = x

        # x = self.code_block(x, self.filters)


        # bottleneck

        # x = self.decode_block(x, self.filters, scale=scale)

        # x = Add()([x, merge_3])

        # x = self.decode_block(x, self.filters, scale=scale)

        # x = self.decode_block(x, self.filters, scale=scale)

        # x = Add()([x, merge_2])

        x = self.code_block(x, self.filters)

        x = self.code_block(x, self.filters)

        x = Add()([x, merge_1])

        x = self.code_block(x, self.filters)

        x = self.code_block(x, 1)

        x = Add()([x, input_tensor])

        x = Conv2D(1, 3, activation='sigmoid', padding='same', kernel_regularizer=l2(self.l2_reg))(x)

        self.model = Model(inputs=input_tensor, outputs=x)
        self.model.summary()
        print('Model created!')

    def create_fca_transposed(self):
        self.kernel_encoder = 3
        self.init_filters = 32
        self.encoder_stride = 2

        shape = self.data.return_trainimage_shape()
        scale = 2

        input_tensor = Input(shape)
        print('input', k.int_shape(input_tensor))

        x = self.code_block(input_tensor, self.init_filters*2^0)

        x = self.code_block(x, self.init_filters*2^0)

        merge_1 = x

        x = self.code_block(x, self.init_filters*2^1)

        x = self.code_block(x, self.init_filters*2^1)

        merge_2 = x

        x = self.code_block(x, self.init_filters*2^2)

        x = self.code_block(x, self.init_filters*2^2)

        merge_3 = x

        x = self.code_block(x, self.init_filters*2^2)

        x = self.code_block(x, self.init_filters*2^3)

        # bottleneck

        x = self.decode_block(x, self.init_filters*2^3, scale=scale)

        x = self.decode_block(x, self.init_filters*2^2, scale=scale)

        x = Add()([x, merge_3])

        x = self.decode_block(x, self.init_filters*2^2, scale=scale)

        x = self.decode_block(x, self.init_filters*2^1, scale=scale)

        x = Add()([x, merge_2])

        x = self.decode_block(x, self.init_filters*2^1, scale=scale)

        x = self.decode_block(x, self.init_filters*2^0, scale=scale)

        x = Add()([x, merge_1])

        x = self.decode_block(x, self.init_filters*2^0, scale=scale)

        x = self.decode_block(x, 1, scale=scale)

        x = Add()([x, input_tensor])

        x = Conv2D(1, 1, activation='sigmoid', padding='same', kernel_regularizer=l2(self.l2_reg))(x)


        self.model = Model(inputs=input_tensor, outputs=x)
        self.model.summary()
        print('Model created!')



    def create_autoencoder_transpose(self):
        self.kernel_encoder = 3
        self.init_filters = 32
        self.encoder_stride = 2

        scale = 2
        shape = self.data.return_trainimage_shape()
        input_tensor = Input(shape)
        print('input', k.int_shape(input_tensor))

        x = self.code_block(input_tensor, 32)

        x = self.code_block(x, 64)

        merge_1 = x

        x = self.code_block(x, 128)

        x = self.code_block(x, 256)

        merge_2 = x

        x = self.code_block(x, 256 * 2)

        x = self.code_block(x, 256 * 2)

        merge_3 = x

        x = self.code_block(x, 256 * 2 * 2)

        x = self.code_block(x, 256 * 2 * 2)

        # bottleneck

        x = self.decode_block(x, 256 * 2 * 2, scale=scale)

        x = self.decode_block(x, 256 * 2, scale=scale)

        x = Add()([x, merge_3])

        x = self.decode_block(x, 256 * 2, scale=scale)

        x = self.decode_block(x, 256, scale=scale)

        x = Add()([x, merge_2])

        x = self.decode_block(x, 128, scale=scale)

        x = self.decode_block(x, 64, scale=scale)

        x = Add()([x, merge_1])

        x = self.decode_block(x, 32, scale=scale)

        x = self.decode_block(x, 1, scale=scale)

        x = Add()([x, input_tensor])

        x = Conv2D(1, 3, activation='sigmoid', padding='same', kernel_regularizer=l2(self.l2_reg))(x)

        self.model = Model(inputs=input_tensor, outputs=x)
        self.model.summary()
        print('Model created!')

    def create_autoencoder_transpose_stride3(self):
        self.kernel_encoder = 3
        shape = self.data.return_trainimage_shape()
        self.init_filters = 32
        scale = 3

        self.encoder_stride = 3

        input_tensor = Input(shape)
        print('input', k.int_shape(input_tensor))

        x = self.code_block(input_tensor, 32)

        x = self.code_block(x, 64)

        merge_1 = x

        x = self.code_block(x, 128)

        x = self.code_block(x, 256)

        merge_2 = x

        x = self.code_block(x, 256 * 2)

        # x = self.code_block(x, 256 * 2)


        # x = self.decode_block(x, 256 * 2, scale=scale)

        x = self.decode_block(x, 256, scale=scale)

        x = Add()([x, merge_2])

        x = self.decode_block(x, 128, scale=scale)

        x = self.decode_block(x, 64, scale=scale)

        x = Add()([x, merge_1])

        x = self.decode_block(x, 32, scale=scale)

        x = self.decode_block(x, 1, scale=scale)

        # x = Lambda(self.upsample_bilinear, arguments={'scale':scale})(x)
        # x = BatchNormalization()(x)

        x = Add()([x, input_tensor])

        x = Conv2D(1, 3, activation='sigmoid', padding='same', kernel_regularizer=l2(self.l2_reg))(x)

        # x = Conv2D(filters,self.kernel_encoder, strides = 1, padding='same')(x)

        # x = Average()([x, input_tensor])

        self.model = Model(inputs=input_tensor, outputs=x)
        self.model.summary()
        print('Model created!')

    def create_model_nopool_12(self):
        print('initializing model: No pooling, crossover, same size')
        kernel_encoder = self.kernel_size
        shape = self.data.return_trainimage_shape()

        input_img = Input(shape)

        print('input', k.int_shape(input_img))

        x = BatchNormalization()(x)

        x = Conv2D(128, kernel_encoder, strides=3, padding='same')(input_img)
        x = LeakyReLU(alpha=0.2)(x)
        x = BatchNormalization()(x)

        x = Conv2D(128, kernel_encoder, strides=3, padding='same')(x)
        x = LeakyReLU(alpha=0.2)(x)
        x = BatchNormalization()(x)

        x_merge1 = x

        x = Conv2D(128, kernel_encoder, strides=3, padding='same')(x)
        x = LeakyReLU(alpha=0.2)(x)
        x = BatchNormalization()(x)

        x = Conv2D(128, kernel_encoder, strides=3, padding='same')(x)
        x = LeakyReLU(alpha=0.2)(x)
        x = BatchNormalization()(x)

        x_merge2 = x

        x = Conv2D(128, kernel_encoder, strides=3, padding='same')(x)
        x = LeakyReLU(alpha=0.2)(x)
        x = BatchNormalization()(x)

        x = Conv2D(128, kernel_encoder, strides=2, padding='same')(x)
        x = LeakyReLU(alpha=0.2)(x)
        x = BatchNormalization()(x)

        # BOTTLENECK MIDPOINT

        x = Conv2DTranspose(128, kernel_encoder, strides=2, padding='same')(x)
        x = LeakyReLU(alpha=0.2)(x)
        # x = Dropout(self.dropout)(x)
        x = BatchNormalization()(x)
        # print('deconv 1', k.int_shape(x))

        x = Conv2DTranspose(128, kernel_encoder, activation='relu', strides=3, padding='same')(x)
        x = LeakyReLU(alpha=0.2)(x)
        x = BatchNormalization()(x)
        # print('deconv 2', k.int_shape(x))

        x = Add()([x, x_merge2])

        x = Conv2DTranspose(128, kernel_encoder, activation='relu', strides=3, padding='same')(x)
        x = LeakyReLU(alpha=0.2)(x)
        # x = Dropout(self.dropout)(x)
        x = BatchNormalization()(x)
        # print('deconv 3', k.int_shape(x))

        x = Conv2DTranspose(128, kernel_encoder, activation='relu', strides=3, padding='same')(x)
        x = LeakyReLU(alpha=0.2)(x)
        x = BatchNormalization()(x)
        # print('deconv 4', k.int_shape(x))

        x = Add()([x, x_merge1])

        x = Conv2DTranspose(128, kernel_encoder, activation='relu', strides=3, padding='same')(x)
        x = LeakyReLU(alpha=0.2)(x)
        # x = Dropout(self.dropout)(x)
        x = BatchNormalization()(x)
        # print('deconv 5', k.int_shape(x))

        x = Conv2DTranspose(128, kernel_encoder, activation='relu', strides=3, padding='same')(x)
        x = LeakyReLU(alpha=0.2)(x)
        x = BatchNormalization()(x)
        # print('deconv 6', k.int_shape(x))

        x = Add()([x, input_img])

        x = Conv2D(1, kernel_encoder, activation='sigmoid', padding='same')(x)

        self.model = Model(inputs=input_img, outputs=x)
        self.model.summary()
        print('Model created!')

