import glob
import pickle
import sys
import os

from skimage.transform import resize

try:
    historyfile = sys.argv[1]
except IndexError:
    print("Usage: videostream.py <path/videofolder> <imagetype> (last is optional)")
    sys.exit(1)# start the program

try:
	imagetype = sys.argv[2]
except:
	imagetype = 'png'

videofiles = glob.glob(os.path.join(historyfile, '*.'+ str(imagetype)), recursive=True)
folder =''
files = {}
for filename in videofiles:
    folders=filename.split('\\')
    videoinstance = (folders[-1].split('.')[0])
    nam,num = videoinstance.split('_')
    num = int(num)
    files[num]=filename



import imageio
with imageio.get_writer(os.path.join(historyfile,'movie.gif'), mode='I') as writer:
    for key in files:
        image= imageio.imread(files[key])

        #print(image.dtype)
        

        image = resize(image,(496,605))

        #print(image.dtype)

        image = (image * 255).astype('uint8')

        writer.append_data(image)
