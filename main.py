from autoencoder import Autoencoder
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '2'

trainfolderpath = os.path.join('..','a4c_data','a4c_rawdata')
labelfolderpath = os.path.join('..','a4c_data','a4c_processed')
# videotrainpath = os.path.join('..','..','a4c_data','a4c_rawdata_videostream')

testfolderpath = os.path.join('..','a4c_data','a4c_raw_test')
testlabelfolderpath = os.path.join('..','a4c_data','a4c_processed_test')

# pretrained_model_path = os.path.join('result_samples','5_l_nos_trans_mse_64fil','model.h5')

# testvideopath = (os.path.join('..','..','a4c_data','US_video_apiclalongaxis'))


from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())


imagetype = 'png'
imagesize=(564,564)

autoencoder = Autoencoder(trainfolderpath=trainfolderpath,labelfolderpath=labelfolderpath, \
                          imagetype=imagetype,imagesize=imagesize)

newtestpath = os.path.join('..','a4c_data','US_heart_test')
newtestpathlabel = os.path.join('..','a4c_data','US_heart_test_processed')

# autoencoder.load(os.path.join('result_samples','4_l_nos_trans_bc64fl','model.h5'))
# autoencoder.testpredictions(newtestpath,newtestpath,'jpg',imagesize,'4_l_nos_trans_bc64fl_test')
# autoencoder.predict_video(testvideopath,'jpg',imagesize,'6_layer_nostride_transposed_bc_32filters_test')

autoencoder.create_fcn_transposed(32,False)
autoencoder.train(epochs=1,batch_size=5)
autoencoder.testpredictions(testfolderpath,testlabelfolderpath,imagetype,imagesize,'fcn_10_layer_564pix_32_filters')

autoencoder.create_fcn_transposed(16,False)
autoencoder.train(epochs=1,batch_size=5)
autoencoder.testpredictions(testfolderpath,testlabelfolderpath,imagetype,imagesize,'fcn_10_layer_564pix_16_filters')

autoencoder.create_fcn_transposed(32,True)
autoencoder.train(epochs=1,batch_size=5)
autoencoder.testpredictions(testfolderpath,testlabelfolderpath,imagetype,imagesize,'fcn_14_layer_564pix_32_filters')

autoencoder.create_fcn_transposed(16,True)
autoencoder.train(epochs=1,batch_size=5)
autoencoder.testpredictions(testfolderpath,testlabelfolderpath,imagetype,imagesize,'fcn_14_layer_564pix_16_filters')
