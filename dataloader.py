import glob
import numpy as np
from PIL import Image
import os

class Dataloader:


    def __init__(self, image_size, w_label, trainpath=None, labelpath = None,imagetype='jpg'):

        self.trainpath = trainpath
        self.labelpath = labelpath

        self.w_label = w_label

        self.trainfiles = []
        self.labelfiles = []

        self.image_size = image_size
        self.imagetype = imagetype

        self.h, self.w = image_size

        self.readfiles()

        self.data = np.ndarray(shape=(self.ntrain,self.h,self.w,1),dtype='uint8')
        if w_label:    self.label = np.ndarray(shape=(self.nlabel,self.h,self.w,1),dtype='uint8')
        self.imagesizes = []
        
        self.readimages()


    def readfiles(self):
        first = False

        #assert isinstance(self.trainpath,str) and not None, 'training path is not a non-empty string'
        #assert isinstance(self.labelpath,str) and not None, 'label path is not a non-empty string'
        #assert isinstance(self.imagetype,str) and not None, 'imagetype is not a non-empty string'
        


        for filename in  glob.glob(os.path.join(self.trainpath,'**','*.'+str(self.imagetype)), recursive=True):
            
            self.trainfiles.append(filename)

        self.ntrain = len(self.trainfiles)

        if self.w_label:
            for filename in glob.glob(os.path.join(self.labelpath,'**','*.'+str(self.imagetype)), recursive=True):
                self.labelfiles.append(filename)

            
            self.nlabel = len(self.labelfiles)

            assert self.ntrain == self.nlabel, 'Number of training examples does not match the number of label examples: Traindata: %d Labeldata: %d' % (self.ntrain,self.nlabel)

            print('Number of data samples found train: %d and label: %d ' % (self.ntrain,self.nlabel))


            #Check if training images matches its corresponding labelimage by name
            for i in range(len(self.trainfiles)):
                assert os.path.split(self.trainfiles[i])[-1] == os.path.split(self.labelfiles[i])[-1], 'Mismatch between training and label filenames' +  self.trainfiles[i] + ' and ' + self.labelfiles[i]

        else:
            print('Number of video samples found: %d' % self.ntrain)


    def readimages(self):

        for index, filename in enumerate(self.trainfiles):

            im2=Image.open(filename)
            imnp = np.array(im2)

            size = imnp.shape
            self.imagesizes.append(size)

            if size != self.image_size:
                imnp = im2.resize((self.image_size[1],self.image_size[0]),Image.ANTIALIAS)
            
            self.data[index,:,:,0] = np.array(imnp)

        if self.w_label:

            for index, filename in enumerate(self.labelfiles):

                im2=Image.open(filename)
                imnp = np.array(im2)

                size = imnp.shape

                if size != self.image_size:
                    imnp = im2.resize((self.image_size[1],self.image_size[0]),Image.ANTIALIAS)
                
                self.label[index,:,:,0] = np.array(imnp)



    def returndata(self):
        if self.w_label: return self.data, self.label
        else: return self.data

    def return_trainimage_shape(self):
        return (self.h,self.w,1)

    def returndatasize(self):
        if self.w_label: assert self.ntrain == self.nlabel, 'Traindata and label data is not the same size'
        return self.ntrain

    def return_trainfiles(self):
        return self.trainfiles